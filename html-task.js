importPackage(Packages.plans);
importPackage(Packages.utils);

trimName = TextUtils.trimName;
capitalize = TextUtils.capitalize;

var departamets = new Departaments("/home/nickl/Yandex.Disk/Dat/DepNames.dat");

var replaceList = {
    "зачет с оценкой": "зачет",
    "УиИвТС": "УИТС"
};

function subst(str){
    return replaceList[str] !== undefined ? replaceList[str]:str;
}

function allSemestsInfo(семестрs) {
    importPackage(Packages.java.io);

    var out = new PrintWriter("sems.html");
    out.println("<html><body>");
    out.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");

    var s = 0;
    семестрs.forEach(function(sem){
        ++s;
        out.println("<h2>Семестр "+s+"</h2>");
        sem.forEach(function(семестр){
            out.println("<p>"+semestrInfo(семестр)+"</p>");
        })
    });


    out.println("</body></html>");
    out.close();
    return "data printed to "+"sems.html";
}


function semestrInfo(семестр) {

    var курсовой = "";
    if(семестр.курсовой().isDefined())
        курсовой = ", "+семестр.курсовой().get();

    var кафедра = "";
    if (departamets.apply(семестр.предмет().кафедра()) != "")
        кафедра = ". Дисциплину закрепить за кафедрой \"" +
        subst(capitalize(trimName(departamets.apply(семестр.предмет().кафедра()), 32), false))
        + "\".";

    return "<b>\""+семестр.предмет().дисциплина()+"\"</b> "+"("+семестр.номер()+"-й семестр) в объёме "+семестр.ЗЕТ()+" з.е., и распределением часов: "+
        семестр.лекций() + " лек., " +
        семестр.лаб() + " лаб., " +
        семестр.парктических() + " пр., " +
        семестр.самостоятельная() + " срс." +
        " форма контроля: " + subst(семестр.контроль()) + курсовой + "" + кафедра

}
