function semestrInfo(семестр){

    var subj =  семестр.предмет();
    var header = "";
    if (!subj.clon())
        header += "\t";
    else
        header += "\t    ";

    header += Semsorter.trimName(subj.дисциплина());
    if(Semsorter.printNI())
        header += " (" + Semsorter.convId(subj.новИдДисциплины()).trim() + ")";

    if (subj.новЦикл().contains("ДВ"))
        header += "(ДВ) ";


    if (subj.семестры().size() > 1)
        header += " " + семестр.номер() + "-й семестр";


    var infoList = [];
    infoList.push(семестр.аудиторная() + "(" + семестр.всегоЧасов() + ") часов");

    if(!isNaN(семестр.ЗЕТ()))
    infoList.push(семестр.ЗЕТ()+" зет");

    if(Semsorter.verboseHours()){
        infoList.push("[" +
        семестр.лекций() + " " +
        семестр.лаб() + " " +
        семестр.парктических() + " " +
        семестр.самостоятельнаяВсеместре() + " " +
        семестр.часЭкз() +
        "]")
    }

    infoList.push(семестр.контроль());
    if(семестр.курсовой().isDefined())
    infoList.push(семестр.курсовой().get());

    return header+":\t"+ infoList.join("\t");
}
