package plans.gui

import java.util.prefs.Preferences
import javax.swing._
import java.awt._
import java.util.concurrent.atomic.AtomicInteger
import java.awt.event._
import utils.SwingUtils._
import java.io._
import utils.{MapPreferences, TextAreaWriter, FiledProperties, WriterOutputStream}
import scala.concurrent.{ExecutionContext, future}
import scala.Console
import javax.swing.text.{TextAction, DefaultEditorKit, JTextComponent}
import plans._
import scala.util.control.Breaks
import java.awt.datatransfer.StringSelection


/**
 * Created with IntelliJ IDEA.
 * User: nickl
 * Date: 13.07.13
 * Time: 17:14
 * To change this template use File | Settings | File Templates.
 */
object PlanValidatorGUI extends PlanValidatorGUIView {

  import ExecutionContext.Implicits.global

//  lazy val props = new FiledProperties(
//    new File(new File(this.getClass.getProtectionDomain.getCodeSource.getLocation.toURI).getParentFile, "conf.properties")
//  )

  lazy val props = new MapPreferences(Preferences.userNodeForPackage(this.getClass))

  def init() {
    createGUI()

    this.exeBtn.addActionListener(exec _)
    this.importBtn.addActionListener(extractCompetentions _)
    this.cmpModCbx.addActionListener(changeCompareMode _ )

    loadAndSaveProperties()

    installContextMenu(frame)
    frame.setVisible(true)
  }


  private[this] def loadAndSaveProperties() {
    for (x <- props.get("frame.x");
         y <- props.get("frame.y");
         width <- props.get("frame.width");
         height <- props.get("frame.height")
    ) {
      frame.setBounds(x.toInt, y.toInt, width.toInt, height.toInt)
    }

    plmField.setText(props.getOrElse("plmPath", ""))
    folderField.setText(props.getOrElse("plansPath", ""))
    cmpModCbx.setSelected(props.get("strictCompare").map(_.toBoolean).getOrElse(false))
    changeCompareMode()

    frame.addWindowListener(new java.awt.event.WindowAdapter() {
      override def windowClosing(windowEvent: java.awt.event.WindowEvent) {

        future {
          val bounds = frame.getBounds
          props("frame.x") = bounds.getX.toInt.toString
          props("frame.y") = bounds.getY.toInt.toString
          props("frame.width") = bounds.getWidth.toInt.toString
          props("frame.height") = bounds.getHeight.toInt.toString
          props("plmPath") = plmField.getText
          props("plansPath") = folderField.getText
          props("strictCompare") = cmpModCbx.isSelected.toString
          props.save()
        }.onComplete {
          case _ => println("finished"); System.exit(0)
        }
      }
    })
  }

  def changeCompareMode(){
    if (cmpModCbx.isSelected) {
      println("mode changed to nonSpaceMapping")
      ProgMapper.defaultMapper = nonSpaceMapping
    }
    else {
      println("mode changed to mixedMapper")
      ProgMapper.defaultMapper = mixedMapper
    }
  }

  def enableExecBtn(enable: Boolean) {
    exeBtn.setEnabled(enable)
    importBtn.setEnabled(enable)
    cmpModCbx.setEnabled(enable)
  }

  def exec() {
    executeTask(ProgValidator.runValidator(new File(plmField.getText), new File(folderField.getText)))
  }

  def extractCompetentions() {
    executeTask(CompetentionExtractor.runExtractor(new File(plmField.getText), new File(folderField.getText)))
  }


  def textAreaPrintStream(): PrintStream = {
    jTextArea.setText("")
    new PrintStream(new WriterOutputStream(new TextAreaWriter(jTextArea)))
  }

  def main(args: Array[String]) {

    import util.control._

    setupNativeLaf()


    invokeInSwing(init())

  }



}

trait PlanValidatorGUIView extends BaseGuiView{

  lazy val frame: JFrame = new JFrame("Синхронизация программ")

  lazy val jTextArea: JTextArea = new JTextArea

  lazy val plmField: JTextField = new JTextField("")

  lazy val folderField: JTextField = new JTextField("")

  lazy val exeBtn: JButton = new JButton("Сравнить")

  lazy val importBtn: JButton = new JButton("Импорт Комп.")

  lazy val cmpModCbx = new JCheckBox("Точные названия")

  def addComponentsToPane(pane: Container) {

    pane.setLayout(new GridBagLayout)
    val r = new AtomicInteger(0)
    addFileComponent(plmField, pane, r.getAndIncrement, "plm-файл", None)
    addFileComponent(folderField, pane, r.getAndIncrement, "Папка с программами", Option(plmField.getText),  true)
    val afterHighGridY = r.get

    def constr = new GridBagConstraints {
      fill = GridBagConstraints.HORIZONTAL
      gridx = 2
      gridy = r.getAndIncrement
    }

    pane.add(exeBtn, constr)
    pane.add(importBtn, constr)
    pane.add(cmpModCbx, constr)

    pane.add(scrollableTextArea(jTextArea), new GridBagConstraints {
      fill = GridBagConstraints.BOTH
      insets = new Insets(2, 2, 2, 2)
      weightx = 1
      weighty = 1
      gridx = 0
      gridy = afterHighGridY
      ipady = 200
      gridheight = r.get() - afterHighGridY + 1
      gridwidth = 2
    })

  }

  protected def createGUI() {
    frame.setLocationRelativeTo(null)
    frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE)
    val contentPane: JPanel = frame.getContentPane.asInstanceOf[JPanel]
    contentPane.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3))
    addComponentsToPane(contentPane)
    frame.pack
  }

}

trait BaseGuiView{

  protected def frame: JFrame

  protected trait TextComponentWrapper{
    def component:Component
    def getText:String
    def setText(str:String)
  }

  implicit class JTextFieldTextComponentWrapper(tf:JTextField) extends TextComponentWrapper {
    def setText(str: String) = tf.setText(str)

    def getText = tf.getText

    def component = tf
  }

  implicit class RememberFieldComponentWrapper(tf:RememberField) extends TextComponentWrapper {
    def setText(str: String) = tf.setText(str)

    def getText = tf.getText

    def component = tf
  }

  protected def parentFileOpt(str: String) = Option(new File(str)).filter(_.exists()).flatMap(f => Option(f.getParentFile))

  protected def addFileComponent(plmField: TextComponentWrapper, pane: Container, i: Int, name: String, pathProvider: => Option[String], folder: Boolean = false): TextComponentWrapper = {
    val c: GridBagConstraints = new GridBagConstraints
    c.fill = GridBagConstraints.HORIZONTAL
    c.gridx = 0
    c.gridy = i
    pane.add(new JLabel(name), c)
    c.gridx = 1
    c.gridy = i
    c.weightx = 1
    c.ipadx = 300
    plmField.component.setSize(50, 50)
    pane.add(plmField.component, c)
    c.gridx = 2
    c.gridy = i
    c.weightx = 0
    c.ipadx = 0
    val fcbtn = new JButton("Выбрать")
    val fc = new JFileChooser()
    if (folder)
      fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY)
    fcbtn.addActionListener((e: ActionEvent) => {
      parentFileOpt(plmField.getText).orElse(pathProvider.flatMap(parentFileOpt)).foreach(f =>
        fc.setCurrentDirectory(f)
      )
      val returnVal = fc.showOpenDialog(frame)
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        plmField.setText(fc.getSelectedFile.getAbsolutePath)
      }
    })
    pane.add(fcbtn, c)
    return plmField
  }

  protected def installContextMenu(comp: Container) {
    for (c <- comp.getComponents()) {
      if (c.isInstanceOf[JTextComponent]) {
        c.addMouseListener(new MouseAdapter() {

          override def mousePressed(e: MouseEvent) {
            this.mouseReleased(e)
          }

          override def mouseReleased(e: MouseEvent) {
            if (e.isPopupTrigger) {
              val component = e.getComponent().asInstanceOf[JTextComponent];
              component.grabFocus()
              val menu = new JPopupMenu();
              var item: JMenuItem = null;
              item = new JMenuItem(new DefaultEditorKit.CopyAction());
              item.setText("Копировать");
              item.setEnabled(component.getSelectionStart() != component.getSelectionEnd());
              menu.add(item);
              item = new JMenuItem(new DefaultEditorKit.CutAction());
              item.setText("Вырезать");
              item.setEnabled(component.isEditable() && component.getSelectionStart() != component.getSelectionEnd());
              menu.add(item);
              item = new JMenuItem(new DefaultEditorKit.PasteAction());
              item.setText("Вставить");
              item.setEnabled(component.isEditable());
              menu.add(item);
              item = new JMenuItem(new  TextAction("copy all") {

                def actionPerformed(e: ActionEvent) {
                  val target: JTextComponent = getTextComponent(e)
                  if (target != null) {
                    val ss = new StringSelection(target.getText());
                    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss,null);
                  }
                }
              });
              item.setText("Копировать всё");
              item.setEnabled(component.isEditable());
              menu.add(item);
              menu.show(e.getComponent(), e.getX(), e.getY());
            }
          }
        });
      } else if (c.isInstanceOf[Container])
        installContextMenu(c.asInstanceOf[Container]);
    }
  }

  protected def enableExecBtn(enable: Boolean)

  protected def textAreaPrintStream(): PrintStream

  protected def executeTask(fu: => Unit) {

    import ExecutionContext.Implicits.global

    enableExecBtn(false)
    val f = future {
      val out = textAreaPrintStream()
      Console.withOut(out) {
        try {
          fu
          out.flush()
        }
        catch {
          case e: Throwable =>
            println(e.getMessage)
            e.printStackTrace(out)
            e.printStackTrace()
        }
      }
    }
    f onComplete {
      case _ => enableExecBtn(true)
    }
  }

  protected def scrollableTextArea(area: JTextArea): JScrollPane = {
    if (area.getFont().getSize < 12)
      area.setFont(area.getFont().deriveFont(12f))
    val scrollPane: JScrollPane = new JScrollPane(area)
    scrollPane.setPreferredSize(new Dimension(450, 110))
    scrollPane
  }

}

