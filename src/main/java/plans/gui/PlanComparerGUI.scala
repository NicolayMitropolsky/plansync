package plans.gui

import java.awt.datatransfer.StringSelection
import java.awt.event.{ActionEvent, MouseEvent, MouseAdapter}
import java.util.prefs.Preferences
import javax.swing._
import java.awt._
import java.util.concurrent.atomic.AtomicInteger
import spantable.SpanTable
import utils._
import java.io.{FileNotFoundException, PrintStream, File}
import scala.concurrent._
import plans._
import utils.SwingUtils._
import java.util
import javax.swing.text.{TextAction, DefaultEditorKit, JTextComponent, DefaultCaret}
import scala.collection.JavaConversions.enumerationAsScalaIterator

/**
 * Created by Nickl on 12.02.14.
 */
object PlanComparerGUI extends PlanComparerGUIView {

  import ExecutionContext.Implicits.global

  //  lazy val props = new FiledProperties(
  //    new File(new File(this.getClass.getProtectionDomain.getCodeSource.getLocation.toURI).getParentFile, "confcomparer.properties")
  //  )

  lazy val props = new MapPreferences(Preferences.userNodeForPackage(this.getClass).node("comparer"))

  val semsorter = new Semsorter(
    verboseHours = true,
    printNI = true
  )

  def init() {
    createGUI()

    this.exeBtn.addActionListener(exec _)

    loadAndSaveProperties()

    installContextMenu(frame)
    frame.setVisible(true)
  }


  private[this] def loadAndSaveProperties() {
    for (x <- props.get("frame.x");
         y <- props.get("frame.y");
         width <- props.get("frame.width");
         height <- props.get("frame.height")
    ) {
      frame.setBounds(x.toInt, y.toInt, width.toInt, height.toInt)
    }

    plm1Field.setText(props.getOrElse("plm1Path", ""))
    plm2Field.setText(props.getOrElse("plm2Path", ""))
    ingoreSRS.setSelected(props.get("ingoreSRS").map(_.toBoolean).getOrElse(false))
    ingoreControl.setSelected(props.get("ingoreControl").map(_.toBoolean).getOrElse(false))

    val gridWidths = props.getOrElse("gridWidths", "")
    for ((w, c) <- gridWidths.split(" ").filter(_.nonEmpty).map(_.toInt)
      zip jtable.getColumnModel.getColumns.toIterable) {
      c.setPreferredWidth(w)
    }

    frame.addWindowListener(new java.awt.event.WindowAdapter() {
      override def windowClosing(windowEvent: java.awt.event.WindowEvent) {

        future {
          val bounds = frame.getBounds
          props("frame.x") = bounds.getX.toInt.toString
          props("frame.y") = bounds.getY.toInt.toString
          props("frame.width") = bounds.getWidth.toInt.toString
          props("frame.height") = bounds.getHeight.toInt.toString
          props("plm1Path") = plm1Field.getText
          props("plm2Path") = plm2Field.getText
          props("ingoreSRS") = ingoreSRS.isSelected.toString
          props("ingoreControl") = ingoreControl.isSelected.toString
          props("gridWidths") = jtable.getColumnModel.getColumns.map(_.getPreferredWidth).mkString(" ")
          props.save()
        }.onComplete {
          case _ => println("finished"); System.exit(0)
        }
      }
    })
  }


  def enableExecBtn(enable: Boolean) {
    exeBtn.setEnabled(enable)
    //    importBtn.setEnabled(enable)
    //    cmpModCbx.setEnabled(enable)
  }

  def exec() {
    //executeTask(ProgValidator.runValidator(new File(plmField.getText), new File(folderField.getText)))

    val file1 = new File(plm1Field.getText)
    val file2 = new File(plm2Field.getText)

    executeTask {
      try {
        println("Старый файл: " + file1.getName)
        println("Новый файл: " + file2.getName)
        println()

        semsorter.printDiff(file1, file2, (sbj1: Предмет, sbj2: Предмет) => {
          sbj1.equals(sbj2, (s1, s2) => s1.equals(s2, !ingoreSRS.isSelected, !ingoreControl.isSelected))
        })


        plm1Field.saveCurrent()
        plm2Field.saveCurrent()
      } catch {
        case e: FileNotFoundException =>
          JOptionPane.showMessageDialog(frame, "Файл не найден:" + e.getMessage, "Ошибка", JOptionPane.ERROR_MESSAGE)
      }
    }
  }


  def textAreaPrintStream(): PrintStream = {
    println("textAreaPrintStream")
    tablemodel.printStream()
  }

  // def textAreaPrintStream(): PrintStream = {
  //    jTextArea.setText("")
  //    new PrintStream(new WriterOutputStream(new TextAreaWriter(jTextArea)))
  //  }

  def main(args: Array[String]) {
    setupNativeLaf()
    invokeInSwing(init())
  }


}


trait PlanComparerGUIView extends BaseGuiView {

  lazy val frame: JFrame = new JFrame("Сравнение учебных планов")

  lazy val tablemodel = new TabSeparatorTableModel(IndexedSeq("Статус", "Название", "Семестр", "Часов", "ЗЕТ", "Часы", "Контроль", "Курсовой"))

  lazy val jtable = {
    val table = new SpanTable(tablemodel) {
      override def getToolTipText(e: MouseEvent): String = {
        val row = rowAtPoint(e.getPoint)
        val column = columnAtPoint(e.getPoint)

        if(row == -1 || column == -1)
          return null

        val colWidth = this.getColumnModel().getColumn(column).getWidth()

        val value = getValueAt(row, column).toString()
        val width = SwingUtilities.computeStringWidth(getFontMetrics(getFont),
          value)

        return if (colWidth > width) null else value
      }
    }
    table.setCellSelectionEnabled(true)
    table.setGridColor(new Color(216, 216, 216))
    table.addMouseListener(new TabSeparatorContextMenu(tablemodel))
    table
  }

  lazy val scrollPane = new JScrollPane(jtable)

  lazy val plm1Field = new RememberField()

  lazy val plm2Field = new RememberField()

  lazy val exeBtn: JButton = new JButton("Сравнить")

  lazy val ingoreSRS = new JCheckBox("Игноировать СРС")

  lazy val ingoreControl = new JCheckBox("Игноировать Контроль")

  def addComponentsToPane(pane: Container) {

    pane.setLayout(new GridBagLayout)
    val r = new AtomicInteger(0)
    addFileComponent(plm1Field, pane, r.getAndIncrement, "plm-файл старый", None)
    addFileComponent(plm2Field, pane, r.getAndIncrement, "plm-файл новый", Option(plm1Field.getText))
    val afterHighGridY = r.get

    def constr = new GridBagConstraints {
      fill = GridBagConstraints.HORIZONTAL
      gridx = 2
      gridy = r.getAndIncrement
    }

    pane.add(exeBtn, constr)
    pane.add(ingoreSRS, constr)
    pane.add(ingoreControl, constr)

    //jTextArea.getCaret.asInstanceOf[DefaultCaret].setUpdatePolicy(DefaultCaret.NEVER_UPDATE)

    pane.add(scrollPane, new GridBagConstraints {
      fill = GridBagConstraints.BOTH
      insets = new Insets(2, 2, 2, 2)
      weightx = 1
      weighty = 1
      gridx = 0
      gridy = afterHighGridY
      ipady = 200
      gridheight = r.get() - afterHighGridY + 1
      gridwidth = 2
    })

  }

  protected def createGUI() {
    frame.setLocationRelativeTo(null)
    frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE)
    val contentPane: JPanel = frame.getContentPane.asInstanceOf[JPanel]
    contentPane.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3))
    addComponentsToPane(contentPane)
    frame.pack
  }

}

class RememberField extends JComboBox[String] {
  val items = new util.Vector[String]()
  private val model = new DefaultComboBoxModel[String](items)
  this.setModel(model)
  this.setEditable(true)

  def getText = this.getSelectedItem.toString

  def setText(str: String) = {
    //items.add(0,str)
    this.setSelectedItem(str)
    //this.setSelectedIndex(0)
  }

  def add(str: String) = {
    model.removeElement(str)
    model.insertElementAt(str, 0)

    if (model.getSize > 5)
      for (x <- 5 until model.getSize) {
        model.removeElementAt(x)
      }

    this.setSelectedIndex(0)
  }

  def saveCurrent() = add(this.getText)

}

class TabSeparatorContextMenu(tablemodel: TabSeparatorTableModel) extends MouseAdapter() {

  override def mousePressed(e: MouseEvent) {
    this.mouseReleased(e)
  }

  override def mouseReleased(e: MouseEvent) {
    if (e.isPopupTrigger) {
      val component = e.getComponent.asInstanceOf[JTable]
      component.grabFocus()
      val menu = new JPopupMenu()
      var item: JMenuItem = null
      item = new JMenuItem(new TextAction("copy") {
        def actionPerformed(e: ActionEvent) {
          val rows = component.getSelectedRows.toIndexedSeq
          val columns = component.getSelectedColumns.toIndexedSeq

          val data = rows
            .map(r => {columns
              .map(c => tablemodel.getValueAt(r, c))
          }
            )
          val ss = new StringSelection(data.map(_.mkString("\t")).mkString("\n"))
          Toolkit.getDefaultToolkit.getSystemClipboard.setContents(ss, null)

        }
      })
      item.setText("Копировать выделенное")
      menu.add(item)
      item = new JMenuItem(new TextAction("copy all") {
        def actionPerformed(e: ActionEvent) {

          val ss = new StringSelection(tablemodel.data.map(_.mkString("\t")).mkString("\n"))
          Toolkit.getDefaultToolkit.getSystemClipboard.setContents(ss, null)

        }
      })
      item.setText("Копировать всё")
      menu.add(item)
      menu.show(e.getComponent, e.getX, e.getY)
    }
  }
}
