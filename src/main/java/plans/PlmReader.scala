package plans

import scala.xml.{Node, NodeSeq, Elem, XML}
import java.io.{StringWriter, PrintWriter, File}
import scala.collection.{mutable, immutable}
import scala.collection.mutable.ListBuffer
import utils.ScalaXMLUtils.Nodeops


object PlmReader {


  def summap(m1: Map[String, Double], m2: Map[String, Double]): Map[String, Double] = {
    (m1.keySet ++ m2.keySet).map(k => (k, m1.getOrElse(k, 0.0) + m2.getOrElse(k, 0.0))).toMap
  }


  val loadParagraphValidator = new LoadParagraphValidator
  val hoursTableValidator = new HoursTableValidator


  def main(args: Array[String]) {

    val filename = """C:\Users\Nickl\Desktop\mystnkodocs\230700_62-01-123456-3417-2.plm.xml"""
    //val filename = """E:\Станкин-доки\УИТС\230100_68-09-000010-3417_Теоретическая информатика.plm.xml"""

    val xml: Elem = XML.loadFile(filename)

    val предметs = loadSubjects(xml)

    for (ustr <- предметs.sortBy(_.дисциплина)) {


      println(ustr.дисциплина)

      val progdata = new ProgData(ustr)
      import progdata._
      println(loadParagraphValidator.genProgTimingParagraph(progdata))

      println("Лекции\tЛабораторные работы\tПрактические занятия\tСамостоятельная работа студентов")
      println(hoursTableValidator.printSemestrsHours(progdata))
      println("В сумме:")
      println((Seq("Лек", "Лаб", "Пр" /*,"СРС"*/).map(attr => attrSum.get(attr).getOrElse("-")) :+ (selfHours)).mkString("\t"))


      println("Общий объём занятий в интерактивной форме составляет "
        + (attrSum.getOrElse("ИнтПр", 0.0) + attrSum.getOrElse("ИнтЛаб", 0.0)) + " часов.")
      println()
    }

  }


  def loadSubjects(file: File): immutable.Seq[Предмет] = loadSubjects(XML.loadFile(file))

  def loadSubjects(file: File, bothChosen: Boolean): immutable.Seq[Предмет] = loadSubjects(XML.loadFile(file), bothChosen)

  def loadSubjects(name: String, bothChosen: Boolean): immutable.Seq[Предмет] = {
    loadSubjects(XML.loadFile(name), bothChosen)
  }

  def loadSubjects(xml: Elem, bothChosen: Boolean = true): immutable.Seq[Предмет] = {
    val strs: NodeSeq = (xml \\ "СтрокиПлана" \ "Строка")

    val предметs: immutable.Seq[Предмет] = for (str <- strs) yield {
      val dissName: String = str.attr("Дис")
      val p = if (bothChosen && str.attribute("НовЦикл").exists(_.head.text.contains("ДВ")) && str.attrOpt("НовИдДисциплины").exists(_.last != '1')) {
        val r = strs.find(n => n.attribute("НовЦикл").exists(_.head.text == str.attribute("НовЦикл").get.head.text) && (n \ "Сем").nonEmpty).get
        new Предмет(r) {
          override val clon = true
          override val дисциплина = dissName
          override val кафедра = str.intAttrOpt("Кафедра")
        }
      }
      else
        new Предмет(str)
      p
    }

    предметs
  }

  @deprecated("use nonSpaceMapping")
  def mapProgs(subjs: Seq[Предмет], dir: Seq[File]) = nonSpaceMapping(subjs: Seq[Предмет], dir: Seq[File])

}


import utils.ScalaXMLUtils.Nodeops

class ProgData(ustr: Предмет) {
  val semesters = ustr.семестры
  val attrSum = ustr.attrSum
  val totalZet = attrSum.get("ЗЕТ").map(_.toInt).getOrElse(0)
  val totalHours: Int = ustr.всегоЧасов
  val hoursInZet: Int = ustr.часовВЗЕТ //.getOrElse(36)
  if (totalZet != totalHours / hoursInZet)
    Console.err.println("зачетные единицы " + totalZet + " != " + (totalHours / hoursInZet) + " in " + ustr.дисциплина)
  val selfHours = (ustr.самостоятельнаяРабота + attrSum.getOrElse("ЧасЭкз", 0.0)).toInt
  val selfZet = selfHours.toDouble / hoursInZet
  val audHours = totalHours - selfHours
  val audZet = audHours.toDouble / hoursInZet
}


class Предмет(n: Node) {
  val дисциплина = n.attr("Дис")
  val sourceДисциплина = n.attr("Дис")
  val дисциплинаДляРазделов = n.intAttrOpt("ДисциплинаДляРазделов")
  val раздел = n.intAttrOpt("Раздел")
  val новЦикл = n.attrOpt("НовЦикл").getOrElse("")
  val новИдДисциплины = n.attrOpt("НовИдДисциплины").getOrElse("")
  val цикл = n.attr("Цикл")
  val идетификаторДисциплины = n.attr("ИдетификаторДисциплины")
  val самостоятельнаяРабота = n.intAttrOpt("СР").getOrElse(0)
  val часовВИнтерактивнойФорме = n.intAttrOpt("ЧасовИнтер").getOrElse(0)
  val всегоЧасов = n.intAttrOpt("ПодлежитИзучению").getOrElse(0)
  val часовВЗЕТ = n.intAttrOpt("ЧасовВЗЕТ").getOrElse(0)
  val семестры = (n \ "Сем").map(n => new Семестр(this, n)).toList

  lazy val номерПервогоСеместра = семестры.headOption.map(_.номер).getOrElse(0)

  val attrSum = (n \ "Сем").map(n => n.attributes.map(a => (a.key, a.value.head.text.toDouble)).toMap).reduceOption(PlmReader.summap).getOrElse(Map.empty)

  val аудиторная = attrSum.getOrElse("Лек", 0.0) + attrSum.getOrElse("Лаб", 0.0) + attrSum.getOrElse("Пр", 0.0)

  val кафедра = n.intAttrOpt("Кафедра")

  val семЗач = n.attrOpt("СемЗач").map(str => str.map(_.toString.toInt)).getOrElse(Vector.empty)
  val семЭкз = n.attrOpt("СемЭкз").map(str => str.map(_.toString.toInt)).getOrElse(Vector.empty)

  val поВыбору = n.attribute("НовЦикл").exists(_.head.text.contains("ДВ"))

  val clon = false

  override def equals(e: Any) = equals(e, _ == _)

  def equals(e: Any, cmp: (Семестр, Семестр) => Boolean): Boolean = {
    e match {
      case p: Предмет =>
        this.дисциплина == p.дисциплина &&
          this.clon == p.clon &&
          //this.новЦикл == p.новЦикл &&
          //this.новИдДисциплины == p.новИдДисциплины &&
          //this.цикл == p.цикл &&
          //this.идетификаторДисциплины == p.идетификаторДисциплины &&
          //        this.самостоятельнаяРабота == p.самостоятельнаяРабота &&
          //        this.часовВИнтерактивнойФорме == p.часовВИнтерактивнойФорме &&
          //        this.всегоЧасов == p.всегоЧасов &&
          //        //this.часовВЗЕТ == p.часовВЗЕТ &&
          //        //this.attrSum == p.attrSum &&
          //        this.аудиторная == p.аудиторная &&
          this.семестры.length == p.семестры.length && (this.семестры zip p.семестры).forall(cmp tupled)
      case _ => false
    }
  }

}

/*
      <Строка Дис="Численные методы" НовЦикл="Б2.В.ДВ.1" НовИдДисциплины="Б2.В.ДВ.1.1" Цикл="Б2.ДВ1"
      ИдетификаторДисциплины="Б2.ДВ1.1" СР="112" ЧасовИнтер="10" ПодлежитИзучению="180" ЧасовВЗЕТ="36" СемЗач="4">
        <Сем Ном="4" Лек="20" Лаб="24" Пр="24" СРС="112" ЗЕТ="5" ИнтЛаб="4" ИнтПр="6" Зач="1" />
      </Строка>

      */

class Семестр(val предмет: Предмет, val n: Node) {
  val номер: Int = n.intAttr("Ном")
  val лекций = n.intAttrOpt("Лек").getOrElse(0)
  val лаб = n.intAttrOpt("Лаб").getOrElse(0)
  val парктических = n.intAttrOpt("Пр").getOrElse(0)
  val аудиторная = лекций + лаб + парктических
  val самостоятельнаяВсеместре = n.intAttrOpt("СРС").getOrElse(0)
  val часЭкз = n.intAttrOpt("ЧасЭкз").getOrElse(0)
  val самостоятельная = самостоятельнаяВсеместре + часЭкз
  val всегоЧасов = аудиторная + самостоятельная
  val ЗЕТ = n.attrOpt("ЗЕТ").map(_.toDouble).getOrElse(Double.NaN)
  val интерактивныхЛаб = n.intAttrOpt("ИнтЛаб").getOrElse(0)
  val интерактивныхПракт = n.intAttrOpt("ИнтЛаб").getOrElse(0)
  val курсоваяРабота = n.attrOpt("КР").isDefined
  val курсовойПроект = n.attrOpt("КП").isDefined
  val курсовой = if (курсоваяРабота) Some("курсовая работа") else if (курсовойПроект) Some("курсовой проект") else None
  val экзамен = n.attrOpt("Экз").isDefined
  val зачет = n.attrOpt("Зач").isDefined
  val зачетСОценкой = n.attrOpt("ЗачО").isDefined
  val контроль = {
    val b = new ListBuffer[String]()
    if (зачет) b += "зачет"
    if (зачетСОценкой) b += "зачет с оценкой"
    if (экзамен) {
      b += "экзамен" +
        (if (часЭкз < 36)
          "(без часов)"
        else "")
    }

    if (b.isEmpty)
      b += "нет контроля"
    b.mkString(", ")
  }

  override def equals(e: Any) = equals(e, true, true)

  def equals(e: Any, srs: Boolean, controlForm: Boolean): Boolean = {
    e match {
      case p: Семестр => this.номер == p.номер &&
        this.лекций == p.лекций &&
        this.лаб == p.лаб &&
        this.парктических == p.парктических &&
        this.аудиторная == p.аудиторная &&
        (!srs
          || this.самостоятельная == p.самостоятельная &&
          this.часЭкз == p.часЭкз &&
          this.всегоЧасов == p.всегоЧасов
          ) &&
        (!controlForm ||
          this.курсоваяРабота == p.курсоваяРабота &&
            this.курсовойПроект == p.курсовойПроект &&
            this.экзамен == p.экзамен &&
            this.зачет == p.зачет &&
            this.контроль == p.контроль)
      case _ => false
    }
  }


}