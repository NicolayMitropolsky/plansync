package plans

import java.nio.channels.Channels
import java.nio.file.{Paths, Path}
import java.util.Collections
import javax.script._

import com.beust.jcommander.{ParameterException, JCommander, Parameter}
import utils.TextUtils

import scala.collection.immutable.Seq
import java.io.{FileInputStream, InputStreamReader, BufferedReader, File}

import scala.collection.mutable.ListBuffer
import scala.io.Source

/**
 * Created with IntelliJ IDEA.
 * User: Nickl
 * Date: 25.06.13
 * Time: 19:56
 * To change this template use File | Settings | File Templates.
 */

object Semsorter {

  def main(args: Array[String]) {

    val ss = new Semsorter

    configureWith(ss, args)
    val filename = ss.file.iterator().next()
    val subjects = PlmReader.loadSubjects(filename, bothChosen = true)
    ss.printBySemestr(subjects)
  }

  private def configureWith(ss : Semsorter, args: Array[String]) {
    val commander = new JCommander()
    try {
      commander.addObject(ss)
      commander.parse(args.toArray: _*)
    }
    catch {
      case p: ParameterException => {
        commander.usage()
        System.exit(0)
      }
    }
  }

}


class Semsorter {

  @Parameter(description = "файл", required = true)
  var file: java.util.List[String] = null

  @Parameter(names = Array("-vh"), description = "часы подробно")
  var verboseHours = false

  @Parameter(names = Array("--printNewId"), description = "печать НовИД")
  var printNI = false

  @Parameter(names = Array("-tn", "--trim-names"), description = "укорачивать названия предметов")
  var tn = false

  @Parameter(names = Array("-si", "--semestr-info-script"), description = "скрипт форматирования семестров")
  var si: String = null

  def this(verboseHours:Boolean = false, printNI:Boolean = false) ={
    this()
    this.verboseHours = verboseHours
    this.printNI = printNI
  }

  lazy val engine = {
    val manager = new ScriptEngineManager()
    val e = manager.getEngineByExtension("js")
    //val engine = manager.getEngineByName("nashorn")

    val simpleBindings = new SimpleBindings()
    simpleBindings.put("Semsorter", this)
    e.setBindings(simpleBindings, ScriptContext.GLOBAL_SCOPE)
    if (si == null || !new File(si).exists()) {

      def defaultScript(): BufferedReader = {
        new BufferedReader(new InputStreamReader(this.getClass.getClassLoader.getResourceAsStream("semsorter.js"), "UTF8"))
      }

      println("cant open semestr-info-script, using following script:")
      val f1 = defaultScript()
      Iterator.continually(f1.readLine()).takeWhile(_ != null).foreach(println)
      f1.close()

      val f2 = defaultScript()
      e.eval(f2)
      f2.close()
    }
    else {
      val f2 = new InputStreamReader(new FileInputStream(si), "UTF8")
      e.eval(f2)
      f2.close()
    }
    e
  }

  def printBySemestr(subjects: Seq[Предмет]) {
    val subjs: Seq[(Предмет, List[Int])] = subjects.map(s => (s, s.семестры.map(_.номер)))

    val grouped:TraversableOnce[TraversableOnce[Семестр]] = for (sem <- 1 to 8) yield {
      for ((subj, sems) <- subjs.filter(t => t._2.contains(sem)).sortBy(_._1.sourceДисциплина)
           if subj.дисциплинаДляРазделов.isEmpty
      /*if subj.кафедра.exists(1 ==)*/ ) yield {
        subj.семестры.zip(Stream.from(1)).find(_._1.номер == sem).get._1
      }
    }

    def processBySelf(): Unit = {
      for ((sems, sem) <- grouped.toSeq.zip(Stream.from(1))) {

        println(sem + "-й семестр:")

        for (семестр <- sems) {
          println(semestrInfo(семестр))
        }

        println()
      }
    }

    if (si != null) try {
      println(engine.asInstanceOf[Invocable].invokeFunction("allSemestsInfo", grouped.map(_.toArray).toArray).asInstanceOf[String]);
    }
    catch {
      case  e: NoSuchMethodException => processBySelf()
    }
    else
      processBySelf()

  }

  private def semestrInfo(семестр: Семестр): String = {
    if (si != null)
      engine.asInstanceOf[Invocable].invokeFunction("semestrInfo", семестр).asInstanceOf[String];
    else
      semestrInfoScala(семестр: Семестр)
  }

  private def semestrInfoScala(семестр: Семестр): String = {

    val subj =  семестр.предмет
    val header = new StringBuilder
    header append (if (!subj.clon) "\t" else "\t    ")
    header append trimName(subj.дисциплина)
    if(printNI)
      header append " (" + convId(subj.новИдДисциплины).trim + ")"

    header append (if (subj.новЦикл.contains("ДВ")) "(ДВ) " else "")
    header append (if (subj.семестры.size > 1) " " + семестр.номер + "-й семестр" else "")

    val infoList = new ListBuffer[String]()
    infoList += семестр.аудиторная + "(" + семестр.всегоЧасов + ") часов"

    infoList += Option(семестр.ЗЕТ).filterNot(_.isNaN).map(_.tryInt + " зет").getOrElse(" ")

    if(verboseHours){
      infoList += "[" +
      семестр.лекций + " " +
        семестр.лаб + " " +
        семестр.парктических + " " +
        семестр.самостоятельнаяВсеместре + " " +
        семестр.часЭкз +
        "]"
    }

    infoList += семестр.контроль
    infoList += семестр.курсовой.getOrElse("")

    header.result()+":\t"+ infoList.filter(_.nonEmpty).mkString("\t")
  }

  def trimName(name: String): String= {
    if(tn )
      TextUtils.trimName(name, 33)
    else
      name
  }

  def printSemestr(семестр: Семестр) = println(semestrInfo(семестр))

  def printDiff(filename1: String, filename2: String, cmpfunk: (Предмет, Предмет) => Boolean) {
    printDiff(new File(filename1), new File(filename2), cmpfunk)
  }

  def printDiff(file1: File, file2: File, cmpfunk: (Предмет, Предмет) => Boolean) {
    val subjects1 = PlmReader.loadSubjects(file1, bothChosen = true)
    val subjects2 = PlmReader.loadSubjects(file2, bothChosen = true)

    printDiff(subjects1, subjects2, cmpfunk)
  }

  def printDiff(subjects1: Seq[Предмет], subjects2: Seq[Предмет], cmpfunk: (Предмет, Предмет) => Boolean = _ == _) {
    val subjs1 = subjects1.filterNot(_.дисциплинаДляРазделов.isDefined)
    val subjs2 = subjects2.filterNot(_.дисциплинаДляРазделов.isDefined)

    def subjKey(s: Предмет): String = {
      //s.дисциплина.replace("???", "").replace("<->", "").trim
      //s.дисциплина.replaceAll( """[^\p{L}\?<>\-]""", "")
      s.дисциплина.replaceAll( """^[\p{L}\d]""", "").replaceAll( """[\?!<\->]""", "")
    }

    val map1 = subjs1.map(s => (subjKey(s), s)).toMap
    val map2 = subjs2.map(s => (subjKey(s), s)).toMap

    val inBoth = map1.keySet & map2.keySet
    val inFirst = map1 -- map2.keySet
    val inSecond = map2 -- map1.keySet

    val (equalPairs, diffPairs) = inBoth.toList.map(k => (map1(k), map2(k))).partition(cmpfunk tupled)

    println("Без изменений:")
    for ((p1, p2) <- equalPairs.sortBy(_._1.номерПервогоСеместра)) {
      println(p2.дисциплина + " (" + convId(p2.новИдДисциплины).trim + ")" + " [" + p2.семестры.map(_.номер).mkString("семестры: ", ",", "") + "; зет: " + p2.семестры.map(_.ЗЕТ).filterNot(_.isNaN).sum.tryInt + "]")
    }

    println()
    println("Изменено:")
    for ((p1, p2) <- diffPairs.sortBy(_._2.номерПервогоСеместра)) {
      //println(p1.дисциплина+" -> "+p2.дисциплина)
      if (p1 != p2) {
        println(p1.дисциплина + " " + p1.семестры.map(_.номер).mkString("(", ",", ")"))
        //println(Seq(p1,p2).map(_.самостоятельнаяРабота))
        println("Было:")
        printsems(p1)
        println("Стало:")
        printsems(p2)
        println()
      }
    }

    val inFirstSems = inFirst.values.flatMap(_.семестры).groupBy(_.номер)
    val inSecondSems = inSecond.values.flatMap(_.семестры).groupBy(_.номер)

    for (semNum <- (inFirstSems.keySet ++ inSecondSems.keySet).toSeq.sorted) {
      println()
      val semsOpt1 = inFirstSems.get(semNum)
      val semsOpt2 = inSecondSems.get(semNum)
      if (semsOpt1.isDefined || semsOpt2.isDefined) {
        println("В семестре " + semNum + ": ")
        semsOpt1.foreach {
          sems1 =>
            println("Убрано:")
            for (p1 <- sems1) {
              printSemestr(p1)
            }
        }
        semsOpt2.foreach {
          sems2 =>
            println("Добавлено:")
            for (p2 <- sems2) {
              printSemestr(p2)
            }
        }
      }
    }
  }

  private[this] def printsems(subj: Предмет) {
    for (семестр <- subj.семестры)
      printSemestr(семестр)
  }

  implicit class tryInt(n: Double) {

    def tryInt =
      if (n.toInt == n) n.toInt.toString else n.toString
  }

  def convId(id: String): String = try {

    val split: Array[String] = id.drop(1).split("\\.")

    //println(split.toList)
    if (split.length > 2)
      (split(1) +: split(0) +: split.drop(2)).mkString(".")
    else
      id
  } catch {
    case e: Throwable => throw new IllegalArgumentException(id, e)
  }

}
