package plans

import java.io._
import utils.ScalaXMLUtils.Nodeops
import scala.collection.mutable.ListBuffer
import utils.docreader.{Table, Document, GenericReader}
import GenericReader._
import scala.util.control.Exception._
import scala.util.matching.Regex

/**
 * Created with IntelliJ IDEA.
 * User: Nickl
 * Date: 30.06.13
 * Time: 15:29
 * To change this template use File | Settings | File Templates.
 */
object ProgValidator {

  def main1(args: Array[String]) {

    //val fname = """C:\Users\Nickl\Desktop\mystnkodocs\230700.62-01(0)-1 Рабочие программы\230700.62(0)-Управление информационными ресурсами и технологиями.doc"""
    // val fname = """C:\Users\Nickl\Desktop\mystnkodocs\230700.62-01(0)-1 Рабочие программы\230700.62-01(0)-1 Базы данных.doc"""

    val file = """/home/nickl/virtualshared/mystnkodocs/230700.62-01(0)-1 Рабочие программы/230700.62-01(0)-1 Базы данных.doc"""

    val предмет = PlmReader.loadSubjects("/home/nickl/virtualshared/mystnkodocs/230700_62-01-123456-3417-6.plm.xml", true).find(_.дисциплина == "Базы данных").get


    val wdDoc = readDocument(new File(file));

    competentionValidator.validate(wdDoc, предмет)

  }

  val paragraphValidator = new LoadParagraphValidator
  val hoursTableValidator = new HoursTableValidator
  val competentionValidator = new CompetentionValidator

  def main(args: Array[String]) {
    val filename = args.lift(0).map(new File(_)).getOrElse(throw new IllegalArgumentException("Укажите plm.xml файл"))
    val dir = args.lift(1).map(new File(_)).getOrElse(throw new IllegalArgumentException("Укажите путь к рабочим программам"))

    runValidator(filename, dir)

  }


  def runValidator(filename: File, dir: File) {
    val progmap: Seq[(Предмет, File)] = mapAndPrintResults(filename, dir)

    println("Результаты сравнения:")
    println()

    for ((subj, file) <- progmap) {
      println(file.getName + " (" + subj.дисциплина + "):")

      val data = new ProgData(subj)



      val wdDoc = readDocument(file)

      if (!wdDoc.paragraphsText.exists(s => s.matches(".*Москва.*2013.*г.*")))
        println("На титульнике не 2013 год")

      paragraphValidator.validate(wdDoc, data)
      hoursTableValidator.validate(wdDoc, subj)

      if (!wdDoc.paragraphsText.exists(s => s.contains("Визы согласования")))
        println("Визы согласования отсутствуют")

      println()
    }
  }


  def mapAndPrintResults(filename: File, dir: File): Seq[(Предмет, File)] = {
    println("plm.xml: " + filename.getPath)
    println("Папка с рабочим программами: " + dir.getPath)
    require(dir.isDirectory, "\"" + dir.getPath + "\" должна являться директорией")
    println()
    val subjs = PlmReader.loadSubjects(filename)

    val progMapResult = ProgMapper.defaultMapper(subjs, dir.listFiles())

    if (progMapResult.dissWithoutProg.nonEmpty)
    {
      println("Дисциплины без программы:\n" + progMapResult.dissWithoutProg.map(_.дисциплина).toSeq.sorted.mkString("\n"))
      println()
    }
    if (progMapResult.progWithoutDiss.nonEmpty)
    {
      println("Программы без дисциплины:\n" + progMapResult.progWithoutDiss.map(_.getName).toSeq.sorted.mkString("\n"))
      println()
    }
    val progmap = progMapResult.matched.toSeq.sortBy(_._1.дисциплина)
    progmap
  }

  def pairmatch(a: Seq[Any], b: Seq[String], pres: Double): Seq[Int] = {
    require(a.length == b.length)
    val r = new ListBuffer[Int]
    for (((at, bt), i) <- a.zip(b).zipWithIndex) {
      at match {
        case at: Int => if (at != bt.toInt) r += i
        case at: Double => if (math.abs(at - bt.replace(',', '.').toDouble) > pres) r += i
      }
    }
    r
  }

}

class LoadParagraphValidator {

  import ProgValidator._

  var pres = 0.05

  // Общая трудоемкость дисциплины составляет 4 зачетных единицы, 144 часа.
  // Из них на обязательную аудиторную нагрузку –60 часов (1,7 зачетных единицы), на самостоятельную работу студента –84 часа (2,3 зачетных единицы).

  private[this] val parreg = ("""Общая трудоемкость дисциплины составляет (\d+) зачетн\p{L}* единиц\p{L}*, (\d+) час\p{L}*\. """ +
    """Из них на обязательную аудиторную нагрузку – (\d+) час\p{L}* \((\d+,?\d*) зачетн\p{L}* единиц\p{L}*\),""" +
    """ на самостоятельную работу студента – (\d+) час\p{L}* \((\d+,?\d*) зачетн\p{L}* единиц\p{L}*\)\.?""").r

  private[this] def preprocParagraph(str: String) = {
    str.replaceAll( """[  ]+""", " ")
  }

  def genProgTimingParagraph(progdata: ProgData): String = {
    import progdata._

    val x: String = f"Общая трудоемкость дисциплины составляет $totalZet%d зачетных единиц, $totalHours%d часов. " +
      f"Из них на обязательную аудиторную нагрузку – $audHours%d часов ($audZet%.2f зачетных единиц)," +
      f" на самостоятельную работу студента – $selfHours%d часов ($selfZet%.2f зачетных единиц)."
    x
  }

  def validate(wdDoc: Document, data: ProgData) {
    val paragraphOption = wdDoc.paragraphsText.map(preprocParagraph).find(p => parreg.pattern.matcher(p).matches())
    if (paragraphOption.isEmpty) {
      println("Абзац про нагрузку не найден или не распознан")
      println("Он должен содержать следующие данные:")
      println(genProgTimingParagraph(data))
    }
    paragraphOption.foreach(
      paragraph => {
        //println(i+": "+paragraph)

        paragraph match {
          case parreg(ze, hours, audhours, audzet, samhours, samzet) => {
            //println("match:" +paragraph/*(ze, hours, audhours, audzet, samhours, samzet)*/)

            val r = pairmatch(
              Seq(data.totalZet, data.totalHours, data.audHours, data.audZet, data.selfHours, data.selfZet),
              Seq(ze, hours, audhours, audzet, samhours, samzet),
              pres = 0.05
            )

            if (r.nonEmpty) {
              println("Абзац про нагрузку:")
              println(paragraph)
              println("содержит ошибики в значениях: " + r.mkString(",") + " ожидалось:")
              println(genProgTimingParagraph(data))
            }
          }
          case _ if paragraph.contains("Общая трудоемкость") => println("unmatched:" + paragraph)
          case _ =>
        }

      })
  }

}

class HoursTableValidator {

  val workTypes = Map(
    "Лекции" -> "Лек",
    "Лабораторные работы" -> "Лаб",
    "Практические занятия" -> "Пр",
    "Самостоятельная работа студентов" -> "СРС"
  )

  def extractHoursFromTable(x: Table): Map[String, Int] = extractHoursFromTable(x: Table, 0)

  def extractHoursFromTable(x: Table, dropRight: Int): Map[String, Int] = {
    val columns = getReadColumns(removeIndexingRow(x.rows))

    val workMap: Map[String, Int] = (for (seq <- columns.map(_.dropWhile(s => !workTypes.keySet(s))).filter(_.nonEmpty)) yield {
      (workTypes(seq.head),
        seq.tail.dropRight(dropRight).flatMap(numifPossible).sum)
    }).toMap
    workMap
  }

  private[this] def tryGetHours(wdDoc: Document, dropRight: Int): Seq[Int] = {
    val hoursMaps: Seq[Map[String, Int]] = wdDoc.tables.map(extractHoursFromTable(_, dropRight)).filter(_.nonEmpty)
    require(hoursMaps.size <= 1)
    val fp = hoursMaps.head.withDefaultValue(0)
    val inProgram = Seq(fp("Лек"), fp("Лаб"), fp("Пр"), fp("СРС"))
    inProgram
  }

  def validate(wdDoc: Document, subj: Предмет) {
    val inProgram = tryGetHours(wdDoc, 0)
    // ("Лек", "Лаб", "Пр" /*,"СРС"*/)
    val at = subj.attrSum.mapValues(_.toInt).withDefaultValue(0)
    val inPlan = Seq(at("Лек"), at("Лаб"), at("Пр"), at("СРС") + at("ЧасЭкз"))
    if (inPlan != inProgram && !(inPlan == tryGetHours(wdDoc, 1) && inPlan == inProgram.map(_ / 2))) {
      println("Часы в таблице не совпадают")
      println("Ожидалось: " + inPlan.mkString("\t"))
      println("Но указано: " + inProgram.mkString("\t"))
      printSemestrsHours(new ProgData(subj))
    }
  }

  def printSemestrsHours(progdata: ProgData) {
    if (progdata.semesters.length > 1) {
      println("По семестрам:")
      for (sem <- progdata.semesters) {
        // Лек="30" Лаб="8" Пр="12" СРС="58"
        println(
          (Seq("Лек", "Лаб", "Пр" /*,"СРС"*/).map(attr => sem.n.attrOpt(attr).getOrElse("-")) :+ (progdata.selfHours.toDouble / progdata.semesters.size)).mkString("\t")
        )
      }
    }
  }
}

class CompetentionValidator {

  def validate(wdDoc: Document, subj: Предмет) {
    val paragraphs = wdDoc.paragraphsText

    val dropWhileOK = paragraphs.dropWhile(!_.contains("Общекультурные компетенции (ОК)")).tail
    val foundOK = dropWhileOK.takeWhile(p => CompetentionExtractor.compreg.findAllMatchIn(p).nonEmpty)
    println(foundOK.size)
    println(foundOK.mkString("\n"))

    val dropWhilePK = paragraphs.dropWhile(!_.contains("Профессиональные компетенции (ПК)")).tail
    val foundPK = dropWhilePK.takeWhile(p => CompetentionExtractor.compreg.findAllMatchIn(p).nonEmpty)
    println(foundPK.size)
    println(foundPK.mkString("\n"))

    val tables = wdDoc.tables

    val (table, row, c) = tables.map(table => {
      val (row, c) = table.rows.map(r => (r, r.cellsTexts("").count(s => s.contains("ОК") || s.contains("ПК")))).maxBy(_._2)
      (table, row, c)
    }
    ).maxBy(_._3)

    println("row=" + row.cellsTexts("").mkString("|"))
  }
}

