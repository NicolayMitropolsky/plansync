package plans

import java.io.File

import scala.io.Source

/**
 * Created by nickl on 30.11.14.
 */
class Departaments(file: File) {

  def this(file:String) = this(new File(file))

  // "/home/nickl/Yandex.Disk/Dat/DepNames.dat"
  
  val names = if(file.exists())
    Source.fromFile(file, "cp1251").getLines().mkString("\n").grouped(100).map(_.trim).toIndexedSeq
  else {
    Console.err.println("Departaments file: "+file.getAbsolutePath+" does not exists")
    IndexedSeq.empty
  }

  def apply(i: Int) = names.lift(i+1).getOrElse("")

  def apply(i: Option[Int]) = {
    //Console.err.println("apply "+i)
    i.flatMap(i=> names.lift(i-1)).getOrElse("")
  }


}



