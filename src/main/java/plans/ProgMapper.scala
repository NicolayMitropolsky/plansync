package plans

import java.io.File
import scala.collection.mutable

/**
 * Created with IntelliJ IDEA.
 * User: nickl
 * Date: 13.07.13
 * Time: 14:03
 * To change this template use File | Settings | File Templates.
 */
trait ProgMapper extends ((Seq[Предмет], Seq[File]) => ProgMapResult) {
  def apply(subjs: Seq[Предмет], files: Seq[File]): ProgMapResult
}

object ProgMapper {
  var defaultMapper: ProgMapper = mixedMapper
}

abstract class PredefinedMappers extends ProgMapper {

  protected type SubjectRepr

  protected type FileRepr

  protected def matches(p: SubjectRepr, fname: FileRepr): Boolean

  def apply(subjs: Seq[Предмет], dir: Seq[File]) = {
    val dnames = new mutable.HashMap[SubjectRepr, Предмет]() ++ subjs.
      map(d => (repSubj(d), d))

    //println("dnames=" + dnames)

    val files = new mutable.HashMap[FileRepr, File]() ++
      dir.filter(file => !file.getName.startsWith("~") && !file.getName.startsWith(".~"))
        .map(f => (repFile(f), f))


    val matched = new mutable.HashMap[Предмет, File]()

    for ((dname, d) <- dnames.toList.sortBy(_._2.дисциплина.length)(Ordering[Int].reverse)) {
      files.find(kv => matches(dname, kv._1)).foreach(kv => {
        matched(d) = kv._2
        files.remove(kv._1)
        dnames.remove(dname)
      })
    }
    new ProgMapResult(matched.toMap, dnames.values, files.values)
  }

  protected def repFile(f: File): FileRepr

  protected def repSubj(d: Предмет): SubjectRepr

}

object cuttingMapping extends CuttingMapper(4)

class CuttingMapper(size: Int) extends PredefinedMappers {

  override type SubjectRepr = List[String]
  override type FileRepr = List[String]

  protected def matches(p: List[String], fname: List[String]) = {
    val i = fname.iterator
    p.forall(s => i.contains(s))
  }

  protected def repFile(f: File) = {
    f.getName.split( """[^\p{L}]""").toList.map(_.take(size).toLowerCase)
  }

  protected def repSubj(d: Предмет) = {
    d.дисциплина.split( """[^\p{L}]""").toList.map(_.take(size).toLowerCase)
  }
}

object nonSpaceMapping extends PredefinedMappers {

  override type SubjectRepr = String
  override type FileRepr = String

  protected def matches(p: SubjectRepr, fname: FileRepr) = fname.contains(p)

  protected def repFile(f: File) = f.getName.toLowerCase.replaceAll( """[^\p{L}]""", "")

  protected def repSubj(d: Предмет) = d.дисциплина.toLowerCase.replaceAll( """[^\p{L}]""", "")
}

object mixedMapper extends ProgMapper{
  def apply(subjs: Seq[Предмет], files: Seq[File]): ProgMapResult = {

    val r1 = nonSpaceMapping(subjs, files)

    val r2 = cuttingMapping(r1.dissWithoutProg.toSeq, r1.progWithoutDiss.toSeq)

    new ProgMapResult(r1.matched ++ r2.matched, r2.dissWithoutProg, r2.progWithoutDiss)
  }
}

class ProgMapResult(val matched: Map[Предмет, File], val dissWithoutProg: Iterable[Предмет], val progWithoutDiss: Iterable[File]) {

  //  for ((s, f) <- matched) {
  //    println(s.дисциплина + " -> " + f.getName)
  //  }

}