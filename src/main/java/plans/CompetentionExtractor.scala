package plans

import utils.docreader.GenericReader
import GenericReader._
import java.io.File
import scala.collection.immutable.TreeSet
import javax.xml.xpath.{XPathConstants, XPathFactory}
import org.w3c.dom.{Document, Attr, Node, NodeList}

import utils.JDKXmlUtils._
import plans.PlmReader._

object CompetentionExtractor {

  def main(args: Array[String]) {
    val plmFile: File = new File("C:\\Users\\Nickl\\Desktop\\mystnkodocs\\230700_62-01-123456-3417-5a.plm.xml")
    val dir = new File( """C:\Users\Nickl\Desktop\mystnkodocs\230700.62-01(0)-1 Рабочие программы""")


    runExtractor(plmFile, dir)
  }


  def runExtractor(plmFile: File, dir: File) {
    val doc = readXml(plmFile)
    val competentionsMap = doc.xpath("Документ/План/Компетенции/Строка").map(n => (n("Индекс"), n("Код"))).toMap

    if(competentionsMap.isEmpty)
      throw new IllegalArgumentException("Кометенции в "+plmFile.getPath+" не загружены")

    val progmap: Seq[(Предмет, File)] = ProgValidator.mapAndPrintResults(plmFile, dir)
    println()

    for ((s, f) <- progmap.toList.sortBy(_._2.getName)) {
      println(f.getName + " -> " + s.дисциплина)
    }

    //println("competentionsMap:" + competentionsMap)

    val subjRecorsMap = doc.xpath("Документ/План/СтрокиПлана/Строка").map(n => (n("Дис"), n)).toMap

    for ((subj, file) <- progmap) {
      val competemtions = extractCompetentions(file)
      //println(competemtions.size + ":" + competemtions)
      val b = subjRecorsMap(subj.дисциплина)
      b("Компетенции") = competemtions.mkString(", ")
      b("КомпетенцииКоды") = competemtions.map(competentionsMap).mkString("&")
    }

    val srcPlmPlainName = plmFile.getName.stripSuffix(".plm.xml")

    val outf = (Iterator("-k") ++ Iterator.from(1).map(i => "-k" + i))
      .map(suf => new File(plmFile.getParentFile, srcPlmPlainName + suf + ".plm.xml"))
      .find(!_.exists()).get

    println()
    println("Сохраняем учебный план с сопоставленными компетенциями")
    println(" в " + outf.getAbsolutePath)
    saveFile(doc, outf)
  }

  val compreg = """(ОК|ПК)-(\d+)""".r

  def extractCompetentions(file: File): TreeSet[String] = {

    val competemtions = (for (x <- readDocument(file).paragraphsText) yield {
      for (mat <- compreg.findAllMatchIn(x)) yield {
        mat.matched
      }
    }).flatten.to[TreeSet]
    competemtions
  }
}
