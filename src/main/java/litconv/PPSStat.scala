package litconv

import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.FileInputStream
import collection.JavaConversions.iterableAsScalaIterable
import collection.JavaConversions.asScalaIterator
import POIUtils._
import org.apache.poi.ss.usermodel.Row

/**
 * Created with IntelliJ IDEA.
 * User: nickl
 * Date: 16.06.13
 * Time: 21:40
 * To change this template use File | Settings | File Templates.
 */
object PPSStat {

  def isDoctor(s:String) = s.contains("д") && s.contains("н")
  def isKandidaye(s:String) = s.contains("к") && s.contains("н")

  def isDocent(s:String) = s.contains("доц")
  def isProf(s:String) = s.contains("проф")

  def hasStep(s:String) = isDoctor(s) || isKandidaye(s)
  def hasZvan(s:String) = isDocent(s) || isProf(s)
  val myKafedrals =
    Set(
      "асоииу",
      "вто",
      "иг",
      "инэб",
      "иняз",
      "ис",
      "итивс",
      "ксу",
      "пм",
      "прим",
      "рим",
      "теормех",
      "уитс",
      "фвис",
      "физики",
      "философии",
      "фм",
      "ээиа"
    )

  val kafNames =
    Map(
      "асоииу" -> "АСОИиУ",
      "вто" -> "ВТО",
      "иг" -> "ИГ",
      "инэб" -> "ИНЭБ",
      "иняз" -> "ИНЯЗ",
      "ис" -> "ИС",
      "итивс" -> "ИТиВС",
      "ксу" -> "КСУ",
      "пм" -> "ПМ",
      "прим" -> "ПриМ",
      "рим" -> "РиМ",
      "теормех" -> "Теормех",
      "уитс" -> "УИТС",
      "фвис" -> "ФВиС",
      "физики" -> "Физики",
      "философии" -> "Философии",
      "фм" -> "ФМ",
      "ээиа" -> "ЭЭиА"
    )

  def main(args: Array[String]) {

    val workbook = new XSSFWorkbook(new FileInputStream(
      "E:\\Станкин-доки\\Аккредитация\\ППС самообследование2.xlsx"))

    val sheet = workbook.getSheetAt(0)

    val rows = sheet.drop(6)

    val repreg = """(\s|кафедра|\(.+)""".r

    val kafString = (v:String) => repreg.replaceAllIn(v.toLowerCase, "")

    def getMyKafFromRow(c: Row): Option[String] = Seq(c.getCell(13),c.getCell(14))
      .map(c => kafString(c.getStringCellValue)).find(myKafedrals)


//    val kafSet = (rows.map(_.getCell(13).getStringCellValue) ++ rows.map(_.getCell(14).getStringCellValue))
//      .map(getKaf).toSet
//    println(kafSet.size)
//    println(kafSet.toSeq.sorted.map(s => "\""+s+ "\"").mkString(", "))

    val kafGroups = rows.groupBy(c => getMyKafFromRow(c))
      .filterKeys(_.nonEmpty)

    for ((kaf,group0) <- kafGroups.toSeq.sortBy(_._1) ) {

      val group = group0.groupBy(r => (
        r.getCell(2).getStringCellValue,
        r.getCell(3).getStringCellValue,
        r.getCell(4).getStringCellValue
        )).values.map(_.head)

      val stepzvan = group.map(
        r => IndexedSeq(r.getCell(7).getStringCellValue,
          r.getCell(8).getStringCellValue))

      println(kafNames(kaf.get) + "\t" +
         group.size+"\t"
         +stepzvan.count(a => hasStep(a(0)) || hasZvan(a(1))) +"\t"
        +stepzvan.count(a => isDoctor(a(0)) || isProf(a(1))))


    }

//    val ychSet = rows.map(_.getCell(8).getStringCellValue).toSet
//
//    ychSet.toList.sorted.foreach(println)

    //println(row.map(_.getTypedValue()).mkString(" - "))




  }

}
