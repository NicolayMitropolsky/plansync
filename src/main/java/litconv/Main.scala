package litconv

import org.apache.poi.xssf.usermodel.{XSSFSheet, XSSFWorkbook}
import java.io.{FileOutputStream, FileInputStream}
import collection.JavaConversions.iterableAsScalaIterable
import collection.JavaConversions.asScalaIterator

import POIUtils._
import org.apache.poi.ss.usermodel.{DateUtil, Sheet, Row, Cell}
import org.apache.poi.ss.util.CellRangeAddress
import scala.collection.immutable.IndexedSeq
import org.apache.poi.hssf.usermodel.HSSFDateUtil
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import System.err.{println => eprintln}
import java.util.concurrent.atomic.AtomicInteger


case class LiterEntry(val bookName: String, val count: Int)

case class SubjLitrEntry(val subjName: String, val literatire: List[LiterEntry])


object Main {

  private implicit def toctanyof(text: String) = new {
    def containsAnyOf(strings: Traversable[String]): Boolean = strings.exists(s => text.contains(s))

    def containsAnyOf(strings: String*): Boolean = strings.exists(s => text.contains(s))
  }

  def mrerioner(sheet: Sheet): (Cell) => Option[CellRangeAddress] = {
    val mergedRegions = (0 until sheet.getNumMergedRegions).map(sheet.getMergedRegion)

    (cell: Cell) => {
      require(cell != null, "cell cant be null")
      mergedRegions
        .find(_.isInRange(cell.getRowIndex, cell.getColumnIndex))
    }
  }

  def parsePage1(sheet1: Sheet): Iterable[SubjLitrEntry] = {
    val rows = sheet1.toIterable //.take(17)

    val mrerion = mrerioner(sheet1)

    val grouped = rows.groupBy(row => mrerion(row.getCell(0))).collect({
      case (Some(group), itr) if group.getLastColumn == 0 => (group, itr.toIndexedSeq)
    })

    val litentries = for ((merg, rows) <- grouped) yield {

      val name = rows(0).getCell(0).getStringCellValue
      val litr = rows.map(_.getCell(2).getStringCellValue).filter(_.nonEmpty)
      val counts = rows.map(_.getCell(3)).collect {case cell if cell.getCellType == Cell.CELL_TYPE_NUMERIC => cell.getNumericCellValue}

      if (litr.size != counts.size)
        System.err.println("litr.size = " + litr.size + " doesnt match counts.size = " + counts.size + " in " + name)

      SubjLitrEntry(name, (litr zip counts).collect {case (n, c) => LiterEntry(n, c.toInt)}.toList)
    }
    litentries
  }

  def parsePage2(sheet1: Sheet): Iterable[SubjLitrEntry] = {
    val rows = sheet1.toIterable //.take(17)

    val mrerion = mrerioner(sheet1)

    val seq: Map[Int, Iterable[Row]] = rows.filter(row => {
      if (row.getCell(0) != null)
        true
      else {
        System.err.println("row " + row.getRowNum + " has null first cell")
        false
      }
    }).groupBy(row => mrerion(row.getCell(0)).map(_.getFirstRow).getOrElse(row.getRowNum): Int)

    val grouped: Seq[(Int, IndexedSeq[Row])] = seq.toSeq.collect({
      case (group, rows) => (group, rows.toIndexedSeq)
    }).sortBy(_._1)

    val litentries = for ((merg, rows) <- grouped) yield {

      val name = rows(0).getCell(0).getStringCellValue
      val litr = rows.map(_.getCell(2).getStringCellValue).filter(_.nonEmpty)
      val counts = rows.map(_.getCell(3)).collect {
        case cell if cell.getCellType == Cell.CELL_TYPE_NUMERIC => cell.getDateCellValue.getMonth + 1
        case cell if cell.getCellType == Cell.CELL_TYPE_STRING => cell.getStringCellValue.replace("-", "").split("/")(0).toDouble
      }

      if (litr.size != counts.size)
        System.err.println("litr.size = " + litr.size + " doesnt match counts.size = " + counts.size + " in " + name)

      SubjLitrEntry(name, (litr zip counts).collect {case (n, c) => LiterEntry(n, c.toInt)}.toList)
    }
    litentries
  }


  def main(args: Array[String]) {

    val workbook = new XSSFWorkbook(new FileInputStream("/home/nickl/virtualshared/Аккредитация/Литератураraw.xlsx"))


    val litentries0: Iterable[SubjLitrEntry] = parsePage1(workbook.getSheetAt(0)) ++ parsePage2(workbook.getSheetAt(1))

    val subjects = workbook.getSheetAt(2).map(_.getCell(0).getStringCellValue)

    val modulesMap = parseSubjects(subjects)
    val litentries = new mutable.HashMap[String,SubjLitrEntry]() ++ litentries0.map(i => (i.subjName,i)) //mapLitEntries(modulesMap.valuesIterator.flatten, litentries0)
    val workbookout = new XSSFWorkbook()
    val osheet = workbookout.createSheet()

    def printLiToRow(row: Row, li: SubjLitrEntry) {
      row.createCell(2).setCellValue(li.subjName)
      row.createCell(3).setCellValue(li.literatire.size)
      row.createCell(4).setCellValue(li.literatire.map(_.count).sum)
      row.createCell(5).setCellValue(li.literatire.map(_.bookName).mkString("\n"))
      row.createCell(6).setCellValue(li.literatire.map(_.count).mkString("\n"))
    }

    val ind = new AtomicInteger(1)

    for ((module, subjects) <- modulesMap) {
      osheet.createRow(osheet.getLastRowNum + 1).createCell(1).setCellValue(module)

      for (subj <- subjects) {
        val row = osheet.createRow(osheet.getLastRowNum + 1)

        val li = litentries.remove(subj)
        row.createCell(0).setCellValue(ind.getAndIncrement)
        row.createCell(1).setCellValue(subj)
        li.foreach(li => {

          if (li.literatire.exists(_.count > 1000))
            eprintln(li)

          printLiToRow(row, li)
        })
      }
    }

    osheet.createRow(osheet.getLastRowNum + 1)
    osheet.createRow(osheet.getLastRowNum + 1)

    litentries.values.foreach(li => printLiToRow(osheet.createRow(osheet.getLastRowNum + 1), li))

    //(0 to osheet.map(_.getLastCellNum).max).foreach(osheet.autoSizeColumn)

    workbookout.write(new FileOutputStream("out.xlsx"))


  }


  def parseSubjects(subjects: Iterable[String]): scala.collection.Map[String, Seq[String]] = {
    val modulesMap = new mutable.LinkedHashMap[String, ListBuffer[String]]

    var curmodule: String = null

    val r = """Б\d(.|:).*""".r.pattern

    for (subj <- subjects) {
      if (r.matcher(subj).matches())
        curmodule = subj
      else
        modulesMap.getOrElseUpdate(curmodule, new ListBuffer[String]) += subj
    }
    modulesMap
  }
}
