package utils.docreader

import java.io.{FileInputStream, InputStream, File}
import org.apache.poi.xwpf.usermodel.{XWPFTableCell, XWPFTableRow, XWPFTable, XWPFDocument}
import collection.JavaConversions.iterableAsScalaIterable

/**
 * Created with IntelliJ IDEA.
 * User: nickl
 * Date: 20.07.13
 * Time: 19:44
 * To change this template use File | Settings | File Templates.
 */
object OOXMLReader extends DocReader {
  def readDocument(file: File) = {
    val fs: InputStream = new FileInputStream(file)

    val doc: XWPFDocument = new XWPFDocument(fs)

    new OOXMLDocument(doc)
  }
}

class OOXMLDocument(val doc: XWPFDocument) extends Document {
  def paragraphsText: IndexedSeq[String] = {
    doc.getParagraphs.map(_.getText.replaceAll( """[\r\u0007]""", """""")).toIndexedSeq
  }

  def tables: IndexedSeq[Table] =
    doc.getTables.map(new OOXMLTable(_)).toIndexedSeq

}

class OOXMLTable(val table: XWPFTable) extends Table {
  def rows: IndexedSeq[TableRow] = table.getRows.map(new OOXMLRow(_)).toIndexedSeq
}

class OOXMLRow(val row: XWPFTableRow) extends TableRow {

  def cells: IndexedSeq[TableCell] = row.getTableCells.map(new OOXMLRowTableCell(_)).toIndexedSeq

}

class OOXMLRowTableCell(val cell: XWPFTableCell) extends TableCell {

  def paragraphsText: IndexedSeq[String] = cell.getParagraphs.map(_.getText).toIndexedSeq

}