package utils.docreader

import java.io.{FileInputStream, File}
import scala.util.control.Exception._


trait DocReader{

  def readDocument(file: File): Document
}

object GenericReader{

  def readDocument(file: File): Document = {
    if(file.getName.endsWith(".doc"))
      MSdocReader.readDocument(file)
    else if(file.getName.endsWith(".docx"))
      OOXMLReader.readDocument(file)
    else throw new IllegalArgumentException("невозможно прочитать файл:"+file.getName)

  }

  def removeIndexingRow(rows1: IndexedSeq[TableRow]): IndexedSeq[TableRow] = {
    rows1.filterNot(r => r.cellsTexts().zip(Stream.from(1)).
      forall(a => catching(classOf[NumberFormatException]).opt(
      a._1.replaceAll("\\D", "").toInt == a._2
    ).getOrElse(false)))
  }

  def getReadColumns(rows: IndexedSeq[TableRow]): IndexedSeq[IndexedSeq[String]] = {
    new IndexedSeq[IndexedSeq[String]] {
      val length: Int = rows.map(_.cells.size).max

      def apply(i: Int): IndexedSeq[String] = new IndexedSeq[String] {
        def length: Int = rows.length

        def apply(j: Int): String = rows(j).cells.lift(i).map(_.paragraphsText.mkString("")).orNull
      }
    }
  }

  def numifPossible(str: String): Option[Int] = {
    catching(classOf[NumberFormatException]).opt(
      str.replaceAll("\\D", "").toInt
    )
  }



}

trait Document {
  def paragraphsText:IndexedSeq[String]
  def tables: IndexedSeq[Table]
}

trait Table {
   def rows: IndexedSeq[TableRow]
}

trait TableRow {

  def cells: IndexedSeq[TableCell]
  def cellsTexts(joiner: String = ""): IndexedSeq[String] = cells.map(_.paragraphsText.mkString(joiner))

}

trait TableCell {

  def paragraphsText: IndexedSeq[String]

}