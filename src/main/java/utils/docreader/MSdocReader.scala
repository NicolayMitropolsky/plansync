package utils.docreader

import org.apache.poi.hwpf.usermodel.{TableCell => HwpfTableCell, TableRow => HwpfTableRow, Table => HwpfTable}
import org.apache.poi.hwpf.{HWPFDocument, usermodel}
import java.io.{FileInputStream, File}
import org.apache.poi.poifs.filesystem.POIFSFileSystem
import scala.util.control.Exception._

/**
 * Created with IntelliJ IDEA.
 * User: Nickl
 * Date: 30.06.13
 * Time: 21:07
 * To change this template use File | Settings | File Templates.
 */
private object MSdocReader extends DocReader {

  def readParagraphsFromDoc(wdDoc: HWPFDocument): Stream[String] = {
    val range = wdDoc.getRange()
    Stream.range(0, range.numParagraphs())
      .map(i => range.getParagraph(i).text().replaceAll( """[\r\u0007]""", """"""))
  }

  def readTablesFromDoc(wdDoc: HWPFDocument): Stream[HwpfTable] = {
    val range = wdDoc.getRange()
    val paragraphs = Stream.range(0, range.numParagraphs())
      .map(i => range.getParagraph(i))

    var curtableLevel = 0
    paragraphs.filter(p => {
      //println("tl:"+p.getTableLevel+" "+p.text())
      if (p.getTableLevel == curtableLevel)
        false
      else {
        curtableLevel = p.getTableLevel;
        p.getTableLevel != 0
      }
    }).map(p => range.getTable(p))
  }

  def readDocument(file: File): Document = {

    val fis = new POIFSFileSystem(new FileInputStream(file));
    val wdDoc = new HWPFDocument(fis);
    new MSdocDocument(wdDoc)
  }
}

import MSdocReader._

class MSdocDocument(wdDoc: HWPFDocument) extends Document {
  def paragraphsText = readParagraphsFromDoc(wdDoc).toIndexedSeq

  def tables: IndexedSeq[Table] = MSdocReader.readTablesFromDoc(wdDoc).map(new MSdocTable(_)).toIndexedSeq
}

class MSdocTable(table: HwpfTable) extends Table {
  def rows: IndexedSeq[MSdocTableRow] = IndexedSeq.range(0, table.numRows()).map(table.getRow).map(new MSdocTableRow(_))
}

class MSdocTableRow(r: HwpfTableRow) extends TableRow {
  def cells: IndexedSeq[MSdocTableCell] = IndexedSeq.range(0, r.numCells()).map(r.getCell).map(new MSdocTableCell(_))
}

class MSdocTableCell(r: usermodel.Range) extends TableCell {
  def paragraphs = IndexedSeq.range(0, r.numParagraphs()).map(r.getParagraph)

  def paragraphsText: IndexedSeq[String] = paragraphs.map(_.text().replaceAll( """[\r\u0007]""", ""))
}

