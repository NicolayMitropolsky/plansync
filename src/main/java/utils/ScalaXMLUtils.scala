package utils

import scala.xml.Node

/**
 * Created by nickl on 15.02.14.
 */
object ScalaXMLUtils {

  implicit class Nodeops(val node: Node) {

    def intAttr(attrname: String) = intAttrOpt(attrname).get

    def intAttrOpt(attrname: String) = node.attribute(attrname).map(_.head.text.toInt)

    def attrOpt(attrname: String) = node.attribute(attrname).map(_.head.text)

    def attr(attrname: String) = attrOpt(attrname: String).get


    def field(fieldName: String) = fieldOpt(fieldName).get

    def intField(fieldName: String) = fieldOpt(fieldName).get.toInt

    def fieldOpt(fieldName: String): Option[String] = (node \ fieldName).headOption.map(_.text)

    def intFieldOtr(fieldName: String) = fieldOpt(fieldName).map(_.toInt)

  }

}
