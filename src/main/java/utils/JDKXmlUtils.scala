package utils

import java.io.File
import org.w3c.dom.{Attr, NodeList, Node, Document}
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult
import javax.xml.xpath.{XPathConstants, XPathFactory}

/**
 * Created with IntelliJ IDEA.
 * User: Nickl
 * Date: 04.07.13
 * Time: 16:43
 * To change this template use File | Settings | File Templates.
 */
object JDKXmlUtils {

  def readXml(plmFile: File): Document = {
    val f = DocumentBuilderFactory.newInstance()
    f.setValidating(false)
    val builder = f.newDocumentBuilder()

    val doc = builder.parse(plmFile)
    doc
  }

  def saveFile(doc: Document, file: File) {
    val transformerFactory = TransformerFactory.newInstance();
    val transformer = transformerFactory.newTransformer();
    val source = new DOMSource(doc);

    val streamResult = new StreamResult(file);
    transformer.transform(source, streamResult);
  }

  implicit class nodeUtils(node:Node){

    lazy val xPathfactory = XPathFactory.newInstance()

    def xpath(query:String):IndexedSeq[Node] = {
      xPathfactory.newXPath().compile(query).evaluate(node, XPathConstants.NODESET).asInstanceOf[NodeList]
    }

    def apply(name:String) = getAttr(name).getOrElse(throw new IllegalArgumentException("no attribute "+name))

    def getAttr(name:String):Option[String] = Option(node.getAttributes.getNamedItem(name)).map(_.getNodeValue)

    def update(name:String, value:String) = setAttr(name:String, value:String)

    def setAttr(name:String, value:String) {
      val attribute: Attr = node.getOwnerDocument.createAttribute(name)
      attribute.setValue(value)
      node.getAttributes.setNamedItem(attribute)
    }


  }

  implicit def nodelistasseq(nl:NodeList):IndexedSeq[Node] = new IndexedSeq[Node]{
    def length: Int = nl.getLength

    def apply(idx: Int): Node = nl.item(idx)
  }

}
