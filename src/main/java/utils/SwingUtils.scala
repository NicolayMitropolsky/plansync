package utils

import java.awt.event.{ActionEvent, ActionListener}
import java.util.prefs.Preferences
import javax.swing.event.{ChangeEvent, ChangeListener}
import java.io._
import javax.swing.{UnsupportedLookAndFeelException, UIManager, JTextArea}
import utils.SwingUtils._
import scala.util.control.Breaks

/**
 * Created with IntelliJ IDEA.
 * User: nickl
 * Date: 16.07.13
 * Time: 17:21
 * To change this template use File | Settings | File Templates.
 */
object SwingUtils {

  implicit def funcToActionListener(f: ActionEvent => Any) = new ActionListener {
    def actionPerformed(e: ActionEvent) {
      f(e)
    }
  }

  implicit def funcToActionListener(f: () => Any) = new ActionListener {
    def actionPerformed(e: ActionEvent) {
      f()
    }
  }

  implicit def funcToActionListener(f: (ChangeEvent) => Any) = new ChangeListener {
    def stateChanged(e: ChangeEvent) {f(e)}
  }

  implicit def funcToRunnable(f:  => Any) = new Runnable {
    def run() {f}
  }

  def invokeInSwing(f: => Any){
    javax.swing.SwingUtilities.invokeLater(new Runnable {
      def run {
        f
      }
    })
  }

  def setupNativeLaf() {
    Breaks.breakable {
      val r = Stream("com.sun.java.swing.plaf.gtk.GTKLookAndFeel", UIManager.getSystemLookAndFeelClassName).foreach(arg =>
        try {
          UIManager.setLookAndFeel(arg)
          Breaks.break()
        }
        catch {
          case e: UnsupportedLookAndFeelException => false
          case e: ClassNotFoundException => false
          case e: InstantiationException => false
          case e: IllegalAccessException => false
        }
      )
    }
  }

}

class Properties extends collection.mutable.Map[String, String] {

  import java.util.{Properties => JProp}
  import collection.JavaConversions.asScalaIterator

  val jprop = new JProp()

  def load(in: InputStream): this.type = {
    jprop.load(in);
    this
  }

  def load(in: Reader): this.type = {
    jprop.load(in);
    this
  }

  def load(in: File): this.type = {
    val ins = new BufferedInputStream(new FileInputStream(in))
    val r = load(new InputStreamReader(ins, "UTF-8"))
    ins.close()
    r
  }

  def load(in: String): this.type = load(new File(in))

  def store(out: OutputStream): this.type = {
    jprop.store(out, "");
    this
  }

  def store(out: Writer): this.type = {
    jprop.store(out, "");
    this
  }

  def store(in: File): this.type = {
    val in1 = new PrintWriter(in, "UTF-8")
    store(in1)
    in1.close()
    this
  }

  def list(in: String): this.type = store(new File(in))

  def +=(kv: (String, String)) = {
    jprop.put(kv._1, kv._2);
    this
  }

  def -=(key: String) = {
    jprop.remove(key);
    this
  }

  def get(key: String) = {
    Option(jprop.get(key).asInstanceOf[String])
  }

  def iterator = jprop.entrySet().iterator().map(kv => (kv.getKey.asInstanceOf[String], kv.getValue.asInstanceOf[String]))
}

class FiledProperties(file: File) extends Properties {

  if (file.exists())
    load(file)

  def save() = store(file)

}

class MapPreferences(prefs: Preferences) extends scala.collection.mutable.Map[String, String]{
  override def +=(kv: (String, String)): this.type = {prefs.put _ tupled kv; this}

  override def -=(key: String): this.type = {prefs.remove(key); this}

  override def get(key: String): Option[String] = Option(prefs.get(key, null))

  override def iterator: Iterator[(String, String)] = prefs.keys().iterator.map(k => (k, this(k)))

  def save() = prefs.sync()
}


class TextAreaWriter(jTextArea: JTextArea) extends Writer {
  def flush() {}

  def write(cbuf: Array[Char], off: Int, len: Int) {
    //jTextArea.getDocument.insertString(0, new String(cbuf.drop(off).take(len)), null)
    val s = new String(cbuf.drop(off).take(len))
    invokeInSwing(jTextArea.append(s))
  }

  def close() {}
}
