package utils;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;

/**
 * Created with IntelliJ IDEA.
 * User: nickl
 * Date: 16.07.13
 * Time: 17:44
 * To change this template use File | Settings | File Templates.
 */
public class WriterOutputStream extends OutputStream {

    private final Writer writer;

    public WriterOutputStream(Writer writer) {
        this.writer = writer;
    }

    public void write(int b) throws IOException {
        // It's tempting to use writer.write((char) b), but that may get the encoding wrong
        // This is inefficient, but it works
        write(new byte[] {(byte) b}, 0, 1);
    }

    public void write(byte b[], int off, int len) throws IOException {
        writer.write(new String(b, off, len));
    }

    public void flush() throws IOException {
        writer.flush();
    }

    public void close() throws IOException {
        writer.close();
    }
}