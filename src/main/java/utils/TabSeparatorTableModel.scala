package utils

import java.io.{PrintStream, Writer}
import javax.swing.table.AbstractTableModel

import spantable.{Span, DefaultSpanModel, SpanModel, SpanTableModel}
import utils.SwingUtils.invokeInSwing

import scala.collection.mutable.ArrayBuffer

/**
 * Created by nickl on 01.11.14.
 */
class TabSeparatorTableModel extends AbstractTableModel with SpanTableModel {
  
  def this(columnsNames: IndexedSeq[String]){
    this()
    this.columnsNames = columnsNames
    setColumnCount(columnsNames.size)
  }
  
  val data = new ArrayBuffer[ArrayBuffer[String]]()

  //private var row = 0
  private var spanModel: SpanModel = new DefaultSpanModel

  private var columns = 0

  var columnsNames = IndexedSeq.empty[String]

  override def getColumnName(column: Int): String = columnsNames.applyOrElse(column, (i:Int) => super.getColumnName(i))

  def setColumnCount(i: Int) = columns = i

  override def getRowCount: Int = data.size

  override def getColumnCount: Int = columns

  override def getValueAt(rowIndex: Int, columnIndex: Int): AnyRef = {
    val orElse1: ArrayBuffer[String] = data.applyOrElse(rowIndex, (i:Int )=> ArrayBuffer.empty)
    val orElse: String = orElse1.applyOrElse(columnIndex, (i:Int ) => "")
    orElse
  }

  def writer(): Writer = new Writer() {

    data.clear()
    spanModel.clear() // = new DefaultSpanModel
    data += new ArrayBuffer[String]()
    //columns = 0

    private val sb = new StringBuilder

    override def flush(): Unit = invokeInSwing {
      for ((row, i) <- data.zipWithIndex; if row.length < columns) {
        spanModel.addSpan(new Span(i, row.length - 1, 1, columns - row.length + 1))
      }
      //TabSeparatorTableModel.this.fireTableStructureChanged()
      TabSeparatorTableModel.this.fireTableDataChanged()
    }

    override def write(cbuf: Array[Char], off: Int, len: Int): Unit = {

      for (i <- off until off + len) {
        val c = cbuf(i)
        if (c == '\t') {
          data.last += sb.result()
          sb.clear()
        } else if (c == '\n') {
          data.last += sb.result()
          sb.clear()
          columns = Math.max(columns, data.last.length)
          data += new ArrayBuffer[String]()
        } else {
          sb.append(c)
        }
      }

    }

    override def close(): Unit = {flush()}
  }

  def printStream() = new PrintStream(new WriterOutputStream(writer()))

  override def getSpanModel: SpanModel = spanModel
}


