package utils

/**
 * Created by nickl on 30.11.14.
 */
object TextUtils {

  def trimName(name: String, l: Int): String= {
    if(name.length < l)
      name
    else {
      getWords(name).map({
        case s if s.length > 2  => s.head.toUpper
        case s => s.head
      }).mkString
    }
  }

  def capitalize(name: String, all: Boolean): String= {
    if(!all)
      name.capitalize
    else {
      name.replaceAll("[,]", "").split("\\s+").filterNot(_.isEmpty).map({
        case s if s.length > 2  => (s.head.toUpper + s.tail)
        case s => s
      }).mkString(" ")
    }
  }

  private def getWords(name: String): Array[String] = {
    name.replaceAll("[,]", "").replaceAll("-", " ").split("\\s+").filterNot(_.isEmpty)
  }
}
