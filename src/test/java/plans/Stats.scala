package plans

import java.io.File

import plans.PlmReader.mapProgs

import scala.collection.immutable.Seq
import scala.math.Ordering
/**
 * Created with IntelliJ IDEA.
 * User: Nickl
 * Date: 26.06.13
 * Time: 18:36
 * To change this template use File | Settings | File Templates.
 */
object Stats {

  def main1(args: Array[String]) {

    val filename = """C:\Users\Nickl\Desktop\mystnkodocs\230700_62-01-123456-3417-3.plm.xml"""

    val subjs = PlmReader.loadSubjects(filename, true)

    val dir = new File("C:\\Users\\Nickl\\Desktop\\mystnkodocs\\230700.62-01(0)-1 Рабочие программы")
    val progmap = mapProgs(subjs, dir.listFiles()).matched

    println(progmap.map(kv => kv._1.дисциплина+" - "+kv._2.getName).zipWithIndex.mkString("\n"))


  }


  def main0(args: Array[String]) {


    val filename = args(0)


    val subjs = PlmReader.loadSubjects(filename, bothChosen = true)

//    val dir = new File("C:\\Users\\Nickl\\Desktop\\mystnkodocs\\230700.62-01(0)-1 Рабочие программы")
//    val progmap = mapProgs(subjs, dir.listFiles()).matched

    val hasProgs = (a:Any) => true //  progmap.keySet

    def lecPersent(s: Предмет): Double = {
      s.attrSum.getOrElse("Лек", 0.0).toDouble / s.аудиторная
    }

    def samPersent(s: Предмет): Double = {
      s.attrSum.getOrElse("СРС", 0.0).toDouble / s.всегоЧасов
    }

    for (предмет <- subjs.filter(hasProgs)
     // filter(lecPersent(_) > 0.25)
      .sortBy(lecPersent)(Ordering[Double].reverse))
    //.sortBy(предмет => предмет.attrSum.getOrElse("Лаб", 0.0)/предмет.семестры.length)(Ordering[Double].reverse))
      //.sortBy(samPersent))
    {
      val sl = предмет.семестры.length
      println(f"${lecPersent(предмет)}%.2f " + предмет.attrSum.getOrElse("Лек", 0.0)/sl +" "+ f"${samPersent(предмет)}%.2f "
        +предмет.attrSum.getOrElse("Лаб", 0.0)/sl+ " "+(предмет.attrSum.getOrElse("Пр", 0.0) + предмет.attrSum.getOrElse("Сем", 0.0))/sl+" " + предмет.дисциплина + " " + предмет.новИдДисциплины + " "
        + предмет.семестры.map(_.номер).mkString("Семестры: ",",",""))


    }

  }

  //short palns
  def main3(args: Array[String]) {

    val filename = """C:\Users\Nickl\Desktop\mystnkodocs\230700_62-01-123456-3417-6.plm.xml"""

    val subjs = PlmReader.loadSubjects(filename, false)

    val out =  for (subj <- subjs) yield {

      val attrSum = subj.attrSum.mapValues(_.toInt).withDefaultValue(0)

      val sems = (1 to 8).map(i => subj.семестры.find(_.номер == i).map(_.ЗЕТ).getOrElse(0.0))

      (subj -> (Seq(subj.всегоЧасов, subj.самостоятельнаяРабота + attrSum("ЧасЭкз"),
        attrSum("Лек")  + attrSum("Лаб") + attrSum("Пр") + attrSum("Сем"),
        attrSum("Лек"),
        attrSum("Лаб"),
        attrSum("Пр"),
        attrSum("Сем")
      ).map(_.toDouble) ++ sems))


      //Map(Пр -> 22, ИнтПр -> 10, ЧасЭкз -> 36, Ном -> 1, ЗЕТ -> 3, Экз -> 1, Лек -> 20, СРС -> 30)
    }

    for (cycle <- out.groupBy(_._1.новЦикл.split('.')(0)).toSeq.sortBy(_._1) ) {
      println(cycle._1)

      val s = cycle._2.filter(_._1.аудиторная > 0).map(_._2).reduce((a1,a2) => a1.zip(a2).map(e => e._1 + e._2))
      println(s.mkString("\t"))

      cycle._2.foreach(su => println(su._1.дисциплина+" " + su._2.mkString("\t")))
      println()
    }

  }


  def main(args: Array[String]) {

    val filename = """C:\Users\Nickl\YandexDisk\УИТС\Планы ФГОС3плюс\09.03.03 Прикладная информатика-26.plm.xml"""

    val subjs = PlmReader.loadSubjects(filename, false)
      .filterNot(_.дисциплина.contains("изическая культура"))
      .filter(_.дисциплинаДляРазделов.isEmpty)

    val semsorter = new Semsorter
    val семестрs = subjs.flatMap(_.семестры)
    for(s <- семестрs.map(s => (s,s.аудиторная.toDouble/s.самостоятельная)).sortBy(_._2)(Ordering[Double].reverse)){
      print(s._2+"\t")
      semsorter.printSemestr(s._1)
    }

    val smslod = семестрs.groupBy(_.номер).mapValues(ss => (ss,ss.map(_.аудиторная).sum.toDouble / ss.map(_.самостоятельная).sum) )


    for(k <- smslod.keys.toSeq.sorted){
      println()
      println("семестр "+k+" "+smslod(k)._2)
      for(s <- smslod(k)._1.map(s => (s,s.аудиторная.toDouble/s.самостоятельная)).sortBy(_._2)(Ordering[Double].reverse)){
        print( ("%.2f".format(s._2))+"\t")
        semsorter.printSemestr(s._1)
      }
    }


  }


}
