//load("nashorn:mozilla_compat.js");

//importPackage(Packages.plans);
//importPackage(Packages.utils);

var TextUtils = Java.type("utils.TextUtils");

trimName = TextUtils.trimName;
capitalize = TextUtils.capitalize;

var departamets = new Packages.plans.Departaments("/Users/nickl-mac/Documents/Yandex.Disk/Dat/DepNames.dat");

var replaceList = {
    "зачет с оценкой": "зачет",
    "УиИвТС": "УИТС"
};

function subst(str) {
    return replaceList[str] !== undefined ? replaceList[str] : str;
}

function allSemestsInfo(семестрs) {

    var out = new java.io.PrintWriter("sems.html");
    out.println("<html><body>");
    out.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");

    var s = 0;
    for each(sem in семестрs){
        ++s;
        out.println("<h2>Семестр " + s + "</h2>");
        for each(семестр in sem){
            out.println("<p>" + semestrInfo(семестр) + "</p>");
        }
    }


    out.println("</body></html>");
    out.close();
    return "data printed to " + "sems.html";
}


function semestrInfo(семестр) {

    var курсовой = "";
    if (семестр.курсовой().isDefined())
        курсовой = ", " + семестр.курсовой().get();

    var кафедра = "";
    if (departamets.apply(семестр.предмет().кафедра()) != "")
        кафедра = ". кафедра \"" +
        subst(capitalize(trimName(departamets.apply(семестр.предмет().кафедра()), 32), false))
        + "\".";

    var name = семестр.предмет().дисциплина();

    var notClon = !семестр.предмет().clon();
    if (notClon)
        name = "<b>\"" + name + "\"</b>";
    else
        name = "&nbsp;&nbsp;&nbsp;&nbsp;<i>альтернатива \"" + name + "\"</i>";

    var zet = "";
    if (семестр.ЗЕТ())
        zet = " " + семестр.ЗЕТ() + " з.е.";

    var additional = "";

    if(notClon)
    additional = " " +zet + ", " + subst(семестр.контроль()) + курсовой + "";

    return name + " " + additional + кафедра

}
